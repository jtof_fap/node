## Node Docker image ##
Autobuilded `node` image base on up-to-date `debian:jessie` (x64 architecture).

### Git branch & Docker tags ###
- `latest` => `10.7.0` / branch master 
- `10.7.0` / branch 10.7
- `8.11.3` / branch 8.11
- `6.14.3` / branch 6.14

### Image build & optimization ###

This image is "one layer image", all `RUN` commands was group into one in order to reduce the final image size. 
Moreover, every `ADD` or `COPY` commands point on an external URL instead of local files. So if you want to rebuild the image, it's not necessary to pull any git repository before, you could just do:

    docker build -t "node:latest" https://bitbucket.org/jtof_fap/node/raw/master/Dockerfile

Node version is an argument (ARG) in Dockerfile, so you could change node version on the fly in build process.

    docker build -t "node:10.7.0" --build-arg NODE_VERSION=10.7.0 https://bitbucket.org/jtof_fap/node/raw/master/Dockerfile

Version numbers are available here: [https://nodejs.org/dist/]

### Image automatic updates ###

This image is automatically rebuilt when: 

- When base image is updated: 

This image is based on an up-to-date debian image (`insecurity/debian:jessie`) and is automatically rebuild when base image is updated. For more information about this process, reports to: 
[https://hub.docker.com/r/insecurity/debian/](https://hub.docker.com/r/insecurity/debian/) 

- When git repository is updated ([https://bitbucket.org/jtof_fap/node/](https://bitbucket.org/jtof_fap/node/)):

### Getting this image ###

    docker pull docker pull insecurity/node:latest
    docker pull docker pull insecurity/node:10.7.0
    docker pull docker pull insecurity/node:8.11.3
    docker pull docker pull insecurity/node:6.14.3

